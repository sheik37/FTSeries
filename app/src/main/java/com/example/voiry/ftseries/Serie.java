package com.example.voiry.ftseries;


public class Serie {

    private Integer ref;
    private String name;
    private Integer episode;
    private Integer saison;
    private String url;


    public Serie(Integer ref,String name, Integer episode,Integer saison,String url){
        this.ref=ref;
        this.saison=saison;
        this.name=name;
        this.episode=episode;
        this.url=url;
    }

    public Integer getSaison() {return  saison;}

    public void setSaison(Integer saison) {
        this.saison = saison;
    }

    public Integer getRef(){
        return this.ref;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String toString(){
        return this.name+" avec l'id:"+this.ref+","+this.saison+","+this.episode;
    }

    public Integer getEpisode(){
        return  this.episode;
    }

    public void setEpisode(Integer episode) {
        this.episode = episode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
