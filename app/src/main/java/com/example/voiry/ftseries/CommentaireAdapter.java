package com.example.voiry.ftseries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;


public class CommentaireAdapter extends ArrayAdapter<Commentaire>{
    public CommentaireAdapter(Context context, List<Commentaire> commentaires) {
        super(context, 0, commentaires);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_commentaire,parent, false);
        }

        CommentaireViewHolder viewHolder = (CommentaireViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new CommentaireViewHolder();
            viewHolder.pseudo = (TextView) convertView.findViewById(R.id.pseudo);
            viewHolder.commentaire = (TextView) convertView.findViewById(R.id.commentaire);
            convertView.setTag(viewHolder);
        }

        Commentaire commentaire = getItem(position);

        viewHolder.pseudo.setText(commentaire.getPseudo());
        viewHolder.commentaire.setText(commentaire.getComm());

        return convertView;
    }

    private class CommentaireViewHolder{
        public TextView pseudo;
        public TextView commentaire;
    }
}
