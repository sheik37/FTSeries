package com.example.voiry.ftseries;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;

public class AjoutCommentaire extends Activity {
    private EditText commentaire;
    private EditText pseudo;
    private SimpleCursorAdapter sca;
    private Cursor cursor;
    private String serie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_commentaire);
        commentaire=(EditText)findViewById(R.id.commentaire);
        pseudo=(EditText)findViewById(R.id.pseudo);
        serie=getIntent().getStringExtra("serie");


        cursor = Accueil.dbopenhelper.getCursorCom();
        String[] columns = {"msg"};
        int[] views = {R.id.text1};
        sca = new SimpleCursorAdapter(this,
                R.layout.simple_list_item_1,
                cursor,
                new String[] {"msg"},
                new int[] { R.id.text1 },
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        );
    }
    public void insertIntoDB( View v ) {
        if (commentaire.getText().length() != 0) {
            Accueil.dbopenhelper.insertValueCom(commentaire.getText().toString(),serie,pseudo.getText().toString());
            cursor.requery();
            sca.notifyDataSetChanged();
        }
        finish();
    }

    @Override
    public void finish(){
        Intent intent=new Intent();
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }
}