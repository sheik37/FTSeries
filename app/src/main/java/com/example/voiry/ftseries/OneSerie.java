package com.example.voiry.ftseries;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class OneSerie extends Activity {
    private Serie serie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_serie);

        TextView name = (TextView) findViewById(R.id.name);
        TextView episode=(TextView)findViewById(R.id.episode);
        TextView saison=(TextView)findViewById(R.id.saison);
        this.serie = Accueil.dbopenhelper.getValuesSerie(getIntent().getStringExtra("serie"));
        name.setText(serie.getName());
        episode.setText(""+serie.getEpisode());
        saison.setText(""+serie.getSaison());
        ImageView image = (ImageView)findViewById(R.id.imageserie);

        Picasso.with(this).load(serie.getUrl()).into(image);

    }
    public void lancerCommentaire(View view) {
        Intent intent = new Intent(this, ListeCommentaire.class);
        intent.putExtra("serie", ""+serie.getRef());
        startActivityForResult(intent, 1);
    }

    public void openWebPage(View view) {
        String url = "https://www.youtube.com/results?search_query=" + serie.getName().toString() + "+bande+annonce";
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    public void supprimer(View view){
        Accueil.dbopenhelper.suppressionSerie(serie.getRef());
        finish();
    }

    public void  retour(View view){
        finish();
    }
    @Override
    public void finish(){
        Intent intent=new Intent();
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }
}

// Format d'une recherche youtube:
// https://www.youtube.com/results?search_query=flash+bande+annonce