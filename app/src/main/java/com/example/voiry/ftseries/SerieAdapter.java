package com.example.voiry.ftseries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;


public class SerieAdapter extends ArrayAdapter<Serie>{
    public SerieAdapter(Context context, List<Serie> series) {
        super(context, 0, series);
    }
    private String nom;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_serie,parent, false);
        }

        SerieViewHolder viewHolder = (SerieViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new SerieViewHolder();
            viewHolder.id = (TextView) convertView.findViewById(R.id.ref);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(viewHolder);
        }

        Serie serie = getItem(position);

        viewHolder.id.setText(""+serie.getRef());
        this.nom=serie.getName();
        viewHolder.name.setText(serie.getName());

        return convertView;
    }

    private class SerieViewHolder{
        public TextView id;
        public TextView name;
    }
}
