package com.example.voiry.ftseries;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.Objects;

public class AjoutSerie extends Activity {

    private Cursor cursor;
    private SimpleCursorAdapter sca;
    private EditText nom;
    private EditText episode;
    private EditText saison;
    private TextView sucess;
    private String name;
    private EditText url;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_serie);

        nom = (EditText)findViewById(R.id.nom);
        episode=(EditText)findViewById(R.id.episode);
        saison=(EditText)findViewById(R.id.saison);
        url=(EditText)findViewById(R.id.url);
        cursor = Accueil.dbopenhelper.getCursorSerie();
        String[] columns = {"nom"};
        int[] views = {R.id.text1};
        sca = new SimpleCursorAdapter(this,
                R.layout.simple_list_item_1,
                cursor,
                new String[] {"nom"},
                new int[] { R.id.text1 },
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        );
        nom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nom.getText().toString().equals("Nom de la série") ){
                    nom.setText("");
                }
            }
        });

    }


    public void insertIntoDB( View v ) {
        // Insertion dans la base de données.
        sucess=(TextView)findViewById(R.id.sucess);
        if( nom.getText().length() != 0 ) {
            if(saison.getText().length()!=0) {
                if (isInteger(saison.getText().toString())){
                    if (episode.getText().length() != 0){
                        if(isInteger(episode.getText().toString())) {
                            if(url.getText().length()!=0) {
                                this.name = nom.getText().toString();
                                this.name = this.name.substring(0, 1).toUpperCase() + this.name.substring(1, this.nom.length());
                                Accueil.dbopenhelper.insertValueSerie(this.name, episode.getText().toString(), saison.getText().toString(),url.getText().toString());
                                cursor.requery();
                                sca.notifyDataSetChanged();
                                sucess.setText(getResources().getString(R.string.sucess));
                                sucess.setTextColor(Color.BLACK);
                            }
                            else{
                                sucess.setText(getResources().getString(R.string.erorurl));
                                sucess.setTextColor(Color.RED);
                            }
                        }
                        else{
                            sucess.setText(getResources().getString(R.string.erorepisodechiffre));
                            sucess.setTextColor(Color.RED);
                        }
                    } else {
                        sucess.setText(getResources().getString(R.string.erorepisode));
                        sucess.setTextColor(Color.RED);
                    }
                }
                else{
                    sucess.setText(getResources().getString(R.string.erorsaisonchiffre));
                    sucess.setTextColor(Color.RED);
                }
            }
            else{
                sucess.setText(getResources().getString(R.string.erorsaison));
                sucess.setTextColor(Color.RED);

            }
        }
        else {
            sucess.setText(getResources().getString(R.string.erorname));
            sucess.setTextColor(Color.RED);
        }
    }


    public void  retour(View view){
        finish();
    }

    @Override
    public void finish(){
        Intent intent=new Intent();
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }

    private static boolean isInteger(String s) {
        boolean isValid = true;
        try{ Integer.parseInt(s); }
        catch(NumberFormatException nfe){ isValid = false; }
        return isValid;
    }

}
