package com.example.voiry.ftseries;

/**
 * Created by ptitb on 20/03/2018.
 */

public class Commentaire {
    private String pseudo;
    private String comm;
    private Integer id;
    private Integer serie;

    Commentaire(Integer id,String comm,String pseudo,Integer serie){
        this.id=id;
        this.pseudo=pseudo;
        this.comm=comm;
        this.serie=serie;
    }

    public Integer getId() {
        return id;
    }

    public Integer getSerie() {
        return serie;
    }

    public String getComm() {
        return comm;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setComm(String comm) {
        this.comm = comm;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setSerie(Integer serie) {
        this.serie = serie;
    }
}
