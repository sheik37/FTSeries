package com.example.voiry.ftseries;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.SimpleCursorAdapter;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;


public class DBopen extends SQLiteOpenHelper {

    private static final String DB_NAME = "Series";// Nom de la base.
    private static final String Serie = "Serie";
    private static final String Commentaire="Commentaire";// Nom de la table.

    private SQLiteDatabase db;// Base de données

    private Context context;

    DBopen(Context context) {
        super(context, DB_NAME, null, 2);
        this.context = context;
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + Serie + " (_id integer primary key autoincrement, nom text not null,episode integer not null,saison integer not null,url text not null);");
        db.execSQL("create table " + Commentaire + " (_id integer primary key autoincrement, msg text not null,serie integer not null,pseudo text not null);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion) {

    }

    /*
     * Insertion d'une chaîne dans la table.
     */
    public void insertValueSerie(String nom, String episode, String saison,String url) {
        // La chaîne n'est pas directement ajoutée dans la base.
        // Il faut passer par la création d'une structure intermédiaire ContenValues.
        ContentValues content = new ContentValues();
        content.put("nom", nom);// Insertion dans la base de l'instance de ContentValues contenant la chapine.
        content.put("episode", parseInt(episode));
        content.put("saison", parseInt(saison));
        content.put("url",url);
        db.insert(Serie, null, content);
    }

    public void insertValueCom(String text,String serie,String pseudo) {
        ContentValues content = new ContentValues();
        content.put("msg", text);
        content.put("serie", parseInt(serie));
        content.put("pseudo",pseudo);// Insertion dans la base de l'instance de ContentValues contenant la chapine.
        db.insert(Commentaire, null, content);
    }


    public List<Serie> getValuesSerie() {
        List<Serie> list = new ArrayList<Serie>();
        String[] columns = {"_id","nom","episode","saison","url"};

        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = db.query(Serie, columns, null, null, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            list.add(new Serie(cursor.getInt(0),cursor.getString(1), cursor.getInt(2), cursor.getInt(3),cursor.getString(4)));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return list;
    }

    public List<Commentaire> getValuesCom() {
        List<Commentaire> list = new ArrayList<Commentaire>();
        String[] columns = {"_id","msg","serie","pseudo"};

        // Exécution de la requête pour obtenir les chaînes et récupération d'un curseur sur ces données.
        Cursor cursor = db.query(Commentaire, columns, null, null, null, null, null);
        // Curseur placé en début des chaînes récupérées.
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Récupération d'une chaîne et insertion dans une liste.
            list.add(new Commentaire(cursor.getInt(0),cursor.getString(1),cursor.getString(3),cursor.getInt(2)));
            // Passage à l'entrée suivante.
            cursor.moveToNext();
        }
        // Fermeture du curseur.
        cursor.close();

        return list;
    }

    public Serie getValuesSerie(String ref){
        Serie serie = null;
        Cursor cursor = db.query(Serie, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if(cursor.getInt(0)==parseInt(ref)) {
                serie = new Serie(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3),cursor.getString(4));
            }
            cursor.moveToNext();

        }
        cursor.close();

        return serie;
    }

    public List<Commentaire> getValuesCom(String serie){
        List<Commentaire> com = new ArrayList<Commentaire>();
        Cursor cursor = db.query(Commentaire, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if(cursor.getInt(2)==parseInt(serie)) {
                com.add(new Commentaire(cursor.getInt(0),cursor.getString(1),cursor.getString(3),cursor.getInt(2)));
            }
            cursor.moveToNext();

        }
        cursor.close();

        return com;
    }

    public Cursor getCursorSerie() {
      String[] columns = {"_id","nom","episode","saison","url"};
      Cursor cursor = db.query(Serie, columns, null, null, null, null, null);
        return cursor;
    }
    public Cursor getCursorCom() {
        String[] columns = {"_id","msg","serie","pseudo"};
        Cursor cursor = db.query(Commentaire, columns, null, null, null, null, null);
        return cursor;
    }

    public SimpleCursorAdapter getAdapterSerie() {
        String[] columns = {"nom"};
        int[] views = { R.id.text1 };
        Cursor cursor = db.query(Serie, columns, null, null, null, null, null);
        return new SimpleCursorAdapter(context,
                R.layout.row_serie,
                cursor,
                columns,
                views);

    }
    public SimpleCursorAdapter getAdapterCom() {
        String[] columns = {"nom"};
        int[] views = { R.id.text1 };
        Cursor cursor = db.query(Commentaire, columns, null, null, null, null, null);
        return new SimpleCursorAdapter(context,
                R.layout.simple_list_item_1,
                cursor,
                columns,
                views);

    }

    public void suppressionSerie(Integer id){
        String colone="_id";
        db.delete(Serie,colone+"="+id,null);
    }

}
