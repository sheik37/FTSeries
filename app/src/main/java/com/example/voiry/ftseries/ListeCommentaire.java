package com.example.voiry.ftseries;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class ListeCommentaire extends Activity {
    private ListView lv0;
    private String serie;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commentaire);
        serie=getIntent().getStringExtra("serie");

        adapter();
    }
    @Override
    protected void onStart() {
        super.onStart();
        adapter();
    }


    public void adapter(){
        lv0 = (ListView)findViewById(R.id.lv0);
        List<Commentaire> commentaires=Accueil.dbopenhelper.getValuesCom(this.serie);
        CommentaireAdapter adapter2 = new CommentaireAdapter(ListeCommentaire.this,commentaires);
        lv0.setAdapter(adapter2);
    }
    public void lancerAjout(View view){
        Intent intent=new Intent(this, AjoutCommentaire.class);
        intent.putExtra("serie", ""+serie);

        startActivityForResult(intent,1);
    }
    public void retour(View view){finish();}

    @Override
    public void finish(){
        Intent intent=new Intent();
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }
}