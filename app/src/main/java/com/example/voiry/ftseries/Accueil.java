package com.example.voiry.ftseries;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Accueil extends Activity {
    static DBopen dbopenhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        dbopenhelper = new DBopen(this);

    }
    public void lancerActivite(View view){
        Intent intent=new Intent(this, ListeSerie.class);
        startActivityForResult(intent,1);
    }
}

//paysage:
//largeur=640
//hauteur=300
//
//portrait:
//largeur=360
//hauteur=570