package com.example.voiry.ftseries;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


public class ListeSerie extends Activity {
    private ListView maList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_serie);
        créationListe();
    }

    @Override
    protected void onStart() {
        super.onStart();
        créationListe();
    }

    public void créationListe(){
        maList = (ListView) findViewById(R.id.lv0);
        List<Serie> series = Accueil.dbopenhelper.getValuesSerie();

        SerieAdapter adapter = new SerieAdapter(ListeSerie.this, series);
        maList.setAdapter(adapter);
        maList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView < ? > parent , View view , int position , long id ) {
                lancerSerie(((Serie)parent.getItemAtPosition(position)).getRef());
            }
        });
    }

    public void lancerSerie(Integer ref){
        Intent intent=new Intent(this, OneSerie.class);
        intent.putExtra("serie", ""+ref);
        startActivityForResult(intent,1);
    }

    public void lancerAjout(View view){
        Intent intent=new Intent(this, AjoutSerie.class);
        startActivityForResult(intent,1);
    }

}
